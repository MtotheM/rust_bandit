use rand::Rng;
use std::io;
use std::io::prelude::*;
use std::fmt;

#[derive(Copy, Clone, Debug)]
enum SlotsValue {
    Cherry,
    Peach,
    Lemon,
    Melon,
    Bannana,
    Strawberry,
    Pineapple,
    Grape,
}

impl SlotsValue {
    fn value(&self) -> String {
        match *self {
            SlotsValue::Cherry => "🍒".to_string(),     // 🍒
            SlotsValue::Peach => "🍑".to_string(),      // 🍑
            SlotsValue::Lemon => "🍋".to_string(),      // 🍋
            SlotsValue::Melon => "🍈".to_string(),      // 🍈
            SlotsValue::Bannana => "🍌".to_string(),    // 🍌
            SlotsValue::Strawberry => "🍓".to_string(), // 🍓
            SlotsValue::Pineapple => "🍍".to_string(),  // 🍍
            SlotsValue::Grape => "🍇".to_string(),      // 🍇
        }
    }
}

#[derive(Debug)]
struct Stats {
    jackpot: i32,
    two_in_a_row: i32,
    wins: i32,
    losses: i32,
    total_winnings: i32,
    total_lost: i32,
}

impl fmt::Display for Stats {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "    Jackpots: {}\nTwo in a row: {}\n        Wins: {}\n      Losses: {}\n    Winnings: {}\n        Bets: {}", self.jackpot, self.two_in_a_row, self.wins, self.losses, self.total_winnings, self.total_lost)
    }
}

struct Player {
    credits: i32,
    stats: Stats,
}

fn rand_fruit() -> SlotsValue {
    let fruits = vec![SlotsValue::Cherry, SlotsValue::Peach, SlotsValue::Lemon, SlotsValue::Melon, SlotsValue::Bannana, SlotsValue::Strawberry, SlotsValue::Pineapple, SlotsValue::Grape];
    rand::thread_rng().choose(&fruits).unwrap().clone()
}

fn pull(credit: i32, player: &mut Player) {
    if player.credits >= credit {
        player.credits -= credit;

        let fruits = vec![rand_fruit(), rand_fruit(), rand_fruit()];
        let code = check(&fruits);
        if code > 0 {
            let win = calc_win(code, credit);
            if code > 10 {
                println!("Result: {} {} {}, JACKPOT! +{}", fruits[0].value(), fruits[1].value(), fruits[2].value(), win);
                player.credits += win;
                player.stats.jackpot += 1;
                player.stats.wins += 1;
                player.stats.total_winnings += win;
            } else {
                println!("Result: {} {} {}, You Win! +{} (two in a row)", fruits[0].value(), fruits[1].value(), fruits[2].value(), win);
                player.credits += win;
                player.stats.two_in_a_row += 1;
                player.stats.wins += 1;
                player.stats.total_winnings += win;
            }
        } else {
            println!("Result: {} {} {}, You Lost :(", fruits[0].value(), fruits[1].value(), fruits[2].value());
            player.stats.losses += 1;
            player.stats.total_lost += credit;
        }
    } else {
        println!("You can't bet '{}' credits when you only have '{}' credits left!", credit, player.credits);
    }
}

fn check(vec: &[SlotsValue]) -> u8 {
    match vec {
        [SlotsValue::Grape, SlotsValue::Grape, SlotsValue::Grape] => 80, // 🍇
        [SlotsValue::Grape, SlotsValue::Grape, _] => 8,
        [_, SlotsValue::Grape, SlotsValue::Grape] => 8,

        [SlotsValue::Strawberry, SlotsValue::Strawberry, SlotsValue::Strawberry] => 70, // 🍓
        [SlotsValue::Strawberry, SlotsValue::Strawberry, _] => 7,
        [_, SlotsValue::Strawberry, SlotsValue::Strawberry] => 7,

        [SlotsValue::Cherry, SlotsValue::Cherry, SlotsValue::Cherry] => 60, // 🍒
        [SlotsValue::Cherry, SlotsValue::Cherry, _] => 6,
        [_, SlotsValue::Cherry, SlotsValue::Cherry] => 6,

        [SlotsValue::Peach, SlotsValue::Peach, SlotsValue::Peach] => 50, // 🍑
        [SlotsValue::Peach, SlotsValue::Peach, _] => 5,
        [_, SlotsValue::Peach, SlotsValue::Peach] => 5,

        [SlotsValue::Pineapple, SlotsValue::Pineapple, SlotsValue::Pineapple] => 40, // 🍍
        [SlotsValue::Pineapple, SlotsValue::Pineapple, _] => 4,
        [_, SlotsValue::Pineapple, SlotsValue::Pineapple] => 4,

        [SlotsValue::Bannana, SlotsValue::Bannana, SlotsValue::Bannana] => 30, // 🍌
        [SlotsValue::Bannana, SlotsValue::Bannana, _] => 3,
        [_, SlotsValue::Bannana, SlotsValue::Bannana] => 3,

        [SlotsValue::Lemon, SlotsValue::Lemon, SlotsValue::Lemon] => 20, // 🍋
        [SlotsValue::Lemon, SlotsValue::Lemon, _] => 2,
        [_, SlotsValue::Lemon, SlotsValue::Lemon] => 2,
        
        [SlotsValue::Melon, SlotsValue::Melon, SlotsValue::Melon] => 1, // 🍈
        [SlotsValue::Melon, SlotsValue::Melon, _] => 10,
        [_, SlotsValue::Melon, SlotsValue::Melon] => 10,

        _ => 0 // Lose
    }
}

fn calc_win(code: u8, credit: i32) -> i32 {
    let price = match code {
        80 => 200, // 🍇🍇🍇
        8  =>  20, // 🍇🍇

        70 =>  72, // 🍓🍓🍓
        7  =>  12, // 🍓🍓

        60 =>  60, // 🍒🍒🍒
        6  =>  10, // 🍒🍒

        50 =>  48, // 🍑🍑🍑
        5  =>   8, // 🍑🍑

        40 =>  36, // 🍍🍍🍍
        4  =>   6, // 🍍🍍

        30 =>  24, // 🍌🍌🍌
        3  =>   4, // 🍌🍌

        20 =>  12, // 🍋🍋🍋
        2  =>   2, // 🍋🍋

        10 =>   6, // 🍈🍈🍈
        1  =>   1, // 🍈🍈

        _ =>  0,   // Making the compiler happy
    };
    let modifier = match credit {
        3 => 1,
        6 => 2,
        9 => 3,
        _ => 0,
    };
    price*modifier
}

fn prompt(player: &mut Player) {
    while player.credits >2 {
        print!("> ");
        io::stdout().flush();

        let mut user_input = String::new();
        io::stdin().read_line(&mut user_input);

        if user_input.trim() == "exit" {
            break
        }

        match user_input.trim() {
            "bet" => pull(3, player),
            "bet 3" => pull(3, player),
            "bet 6" => pull(6, player),
            "bet 9" => pull(9, player),
            "exit" => break,
            _ => println!("Invalid input, try again!")
        }
        println!("Current Credits: {}", player.credits);
    }
    if player.credits <3 {
        println!("Game Over! Out of Credits!")
    }
}


fn main() {
    let mut player = Player { credits: 300, stats: Stats { jackpot: 0, two_in_a_row: 0, wins: 0, losses: 0, total_winnings: 0, total_lost: 0 } };
    println!{"Welcome to Rust Bandit 0.1!\n\nThe rules in this game is simple. you start with {} credits.\nand you get to play until you run out of credits,\neach spin cost 3 credits, You can double or triple your bet\n\nCurrent Credits: {} (Help: 'bet 3/6/9', default is 3, 'exit')", player.credits, player.credits}
    prompt(&mut player);
    println!("{}", player.stats);
}